<h1 align="center">Hi 👋 I'm Anushka</h1>
<h3 align="center">
I am deeply passionate about the transformative power of technology and its limitless potential. A Final year student studying Advanced Computer Science at Monash University, Melbourne, Australia, with almost 5 years of experience in Software Development, Research in Technology, IT Leadership, and Programming Tutor positions. I've been focused on delivering innovative solutions and ensuring they meet end-user needs. This means I'm always learning new technology and the business aspects. I aim to get results that benefit both the business and the end-users.
</h3>

<h3 align="center"> I'm eager to connect with like-minded professionals and explore opportunities where we can harness technology to make a meaningful impact. Let's innovate together. Connect with me: www.linkedin.com/in/anushka-reddy</h3>

<h5 align="left">Most of my repositories are private. Please ask for access here: anushkar614@gmail.com</h5>

<p align="left">
</p>

<h3 align="left"> Programming Languages:</h3>
<p align="left">
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a>
<a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40"/> </a>
<a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a>
<a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="50" height="40"/> </a>
<a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="50" height="50"/> </a>
<a href="https://www.haskell.org/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Haskell-Logo.svg" alt="haskell" width="40" height="40"/> </a>
<a href="https://en.wikipedia.org/wiki/AWK" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/The-AWK-Programming-Language.svg/1200px-The-AWK-Programming-Language.svg.png" alt="awk logo" width="100" height="45"/> </a>
<a href="https://www.doc.ic.ac.uk/lab/secondyear/spim/node9.html" target="_blank" rel="noreferrer"> <img src="https://cdn.freebiesupply.com/logos/large/2x/mips-logo-png-transparent.png" alt="mipsr2000 logo" width="65" height="65"/> </a>
</p>

<h3 align="left">Web Development:</h3>
<a href="https://nodejs.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="70" height="50"/> </a>
<a href="https://angular.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/angular/angular-ar21.svg" alt="angularjs" width="95" height="50"/> </a>
<a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="40" height="40"/> </a>
<a href="https://spring.io/projects/spring-boot" target="_blank" rel="noreferrer"> <img src="https://e4developer.com/wp-content/uploads/2018/01/spring-boot.png" alt="springboot logo" width="80" height="45"/> </a> 
<a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a>
<a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a>
<a href="https://jquery.com/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/JQuery-Logo.svg/2560px-JQuery-Logo.svg.png" alt="jquery" width="65" height="20"/> </a>
<a href="https://getbootstrap.com/" target="_blank" rel="noreferrer"> <img src="https://seeklogo.com/images/B/bootstrap-logo-69A1CCC10B-seeklogo.com.png" alt="bootstrap" width="85" height="20"/> </a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://jscolor.com/img/jscolor-box.png" alt="jscolor logo" width="85" height="60"/> </a>
<a href="https://rxjs.dev/" target="_blank" rel="noreferrer"> <img src="https://rxjs.dev/generated/images/marketing/home/Rx_Logo-512-512.png" alt="Rxjs logo" width="40" height="40"/> </a>
<a href="https://www.json.org/json-en.html" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/json/json-ar21.svg" alt="json" width="70" height="40"/> </a>
<a href="https://www.ibm.com/topics/rest-apis#:~:text=A%20REST%20API%20(also%20called,transfer%20(REST)%20architectural%20style." target="_blank" rel="noreferrer"> <img src="https://uxwing.com/wp-content/themes/uxwing/download/web-app-development/rest-api-icon.png" alt="REST APIs logo" width="50" height="40"/> </a>
<a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a>
<a href="https://docs.mapbox.com/api/overview/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/mapbox/mapbox-ar21.svg" alt="mapbox" width="90" height="45"/>
<a href="https://www.lucidchart.com/pages/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/lucidchart/lucidchart-ar21.svg" alt="lucidchart" width="90" height="45"/> </a>
<a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a>

<h3 align="left">Data and Databases:</h3>
<p> <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="60" height="55"/> </a>
<a href="https://www.oracle.com/au/database/technologies/appdev/plsql.html" target="_blank" rel="noreferrer"> <img src="https://www.hatthieves.es/wp-content/uploads/2019/09/16-420x300.png" alt="pl/sql logo" width="60" height="50"/> </a>
<a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="50" height="90"/> </a>
<a href="https://www.oracle.com/au/database/nosql/" target="_blank" rel="noreferrer"> <img src="https://www.javacodegeeks.com/wp-content/uploads/2018/12/3-300x300.png" alt="nosql" width="60" height="55"/> </a>
<a href="https://www.oracle.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/oracle/oracle-original.svg" alt="oracle" width="60" height="55"/> </a>
</p>
<p>
<h3 align="left">Libraries:</h3>
<a href="https://flask.palletsprojects.com/en/3.0.x/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Flask_logo.svg/1200px-Flask_logo.svg.png" alt="flask" width="100" height="40"/> </a>
<a href="https://pytorch.org/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/PyTorch_logo_black.svg/1280px-PyTorch_logo_black.svg.png" alt="pytorch" width="100" height="25"/> </a>
<a href="https://numpy.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/numpy/numpy-ar21.svg" alt="NumPy" width="70" height="40"/> </a>
<a href="https://pandas.pydata.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/2ae2a900d2f041da66e950e4d48052658d850630/icons/pandas/pandas-original.svg" alt="pandas" width="40" height="40"/> </a>
</p>

<h3 align="left">Cloud and DevOps:</h3>
<a href="https://aws.amazon.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/amazonwebservices/amazonwebservices-original-wordmark.svg" alt="aws" width="40" height="40"/></a>
<a href="https://azure.microsoft.com/en-in/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/microsoft_azure/microsoft_azure-ar21.svg" alt="azure" width="90" height="50"/> </a>
<a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/docker/docker-original.svg" alt="docker logo" width="50" height="50"/></a>
<a href="https://bitbucket.org/product" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bitbucket/bitbucket-original.svg" alt="bitbucket logo" width="40" height="50"/></a>
<a href="https://www.atlassian.com/software/bamboo" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bamboo/bamboo-original.svg" alt="bamboo" width="40" height="40"/> </a>
<a href="https://about.gitlab.com/free-trial/devsecops/?utm_medium=cpc&utm_source=bing&utm_campaign=brand_apac_pr_rsa_br_exact&utm_content=/free-trial/devsecops/&_bt=&_bk=gitlab&_bm=e&_bn=s&_bg=1321614626779608&msclkid=925d83dce6a11cb71d25e607df568f05" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original.svg" alt="gitlab logo" width="40" height="40"/></a>
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" alt="git logo" width="40" height="40"/></a>

<h3 align="left"> Development Environments and Tools:</h3>
<p align="left">
<a href="https://jasmine.github.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/jasmine/jasmine-icon.svg" alt="jasmine" width="40" height="40"/> </a> 
<a href="https://www.wireshark.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/wireshark/wireshark-ar21.svg" alt="wireshark" width="80" height="40"/> </a>
<a href="https://sourceforge.net/projects/tuataratmsim/files/tuataratmsim/Version%201.0/" target="_blank" rel="noreferrer"> <img src="https://images.squarespace-cdn.com/content/v1/5be5561e1137a68b3d236e4f/1559235070910-ZS72N2QX70QNDRVIC608/logo+2.png" alt="turing machine logo" width="90" height="30"/> </a>
<a href="https://en.wikipedia.org/wiki/Yacc" target="_blank" rel="noreferrer"> <img src="https://guru-home.dyndns.org/lex-yacc/html/img/YACC-Compiler.jpg" alt="yacc compiler logo" width="120" height="50"/> </a>
<a href="https://www.geeksforgeeks.org/what-is-lex-in-compiler-design/" target="_blank" rel="noreferrer"> <img src="https://cdn.ecommercedns.uk/files/4/228344/5/6538365/lex-logo-w300.jpg" alt="lex compiler logo" width="50" height="45"/> </a>
<a href="https://www.jetbrains.com/idea/" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/intellij/intellij-original.svg" alt="intellij IDEA logo" width="40" height="40"/> </a>
<a href="https://www.jetbrains.com/pycharm/" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/pycharm/pycharm-original.svg" alt="pyCharm logo logo" width="40" height="40"/> </a>
<a href="https://jupyter.org/" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jupyter/jupyter-original.svg" alt="jupyter logo" width="40" height="40"/> </a>

<h3 align="left">Project management and Collaboration:</h3>
<a href="https://www.atlassian.com/software/jira" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jira/jira-original.svg" alt="jira logo" width="40" height="40"/> </a>
<a href="https://www.atlassian.com/software/confluence" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/confluence/confluence-original.svg" alt="confluence logo" width="40" height="40"/></a>
<a href="https://en.wikipedia.org/wiki/Notion_(productivity_software)" target="_blank" rel="noreferrer"> <img src="https://www.pngall.com/wp-content/uploads/15/Notion-Logo-Transparent.png" alt="notion logo" width="40" height="40"/></a>
<a href="https://trello.com/" target="_blank" rel="noreferrer"> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/trello/trello-plain.svg" alt="trello logo" width="40" height="40"/> </a>
<a href="https://slack.com/intl/en-au" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/slack/slack-ar21.svg" alt="slack logo" width="" height="40"/> </a>
</p>